package com.sgktesvik.repository;

import com.sgktesvik.entity.UserMerchant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserMerchantRepository extends JpaRepository<UserMerchant, Long> {
//    @Query(value = "select um from UserMerchant um where um.calculationStatus = :status order by id asc limit 1")
    UserMerchant findFirstByCalculationStatusOrderByIdAsc(int calculationStatus);
}
