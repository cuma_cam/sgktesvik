package com.sgktesvik.repository;

import com.sgktesvik.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findUserByApprovalGuid(String guid);
}
