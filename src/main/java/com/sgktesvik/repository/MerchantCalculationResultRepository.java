package com.sgktesvik.repository;

import com.sgktesvik.entity.MerchantCalculationResult;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created on 5.04.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
public interface MerchantCalculationResultRepository extends JpaRepository<MerchantCalculationResult, Long> {
}
