package com.sgktesvik.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * Created on 5.04.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
@Entity
@Data
@NoArgsConstructor
@Table(name = "merchant_calculation_result")
public class MerchantCalculationResult {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "merchant_calculator_result_generator")
    @SequenceGenerator(name = "merchant_calculator_result_generator", sequenceName = "merchant_calculation_result_seq")
    private Long id;

    private String calculationType;
    private BigDecimal refundAmount;
    private byte[] zipData;

    @JoinColumn(name = "user_merchant_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private UserMerchant userMerchant;
}
