package com.sgktesvik.entity;

import com.sgktesvik.util.PasswordColumnConverter;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;

/**
 * IsYeri clası
 *
 * @author Cuma Cam
 */
@Entity
@Data
@NoArgsConstructor
@Table(name = "user_merchant")
public class UserMerchant {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_merchant_generator")
    @SequenceGenerator(name = "user_merchant_generator", sequenceName = "user_merchant_seq")
    private Long id;
    private String sgkRegistryNoKey; // 21419010111344790101048000
    private String sgkRegistryNo; // 214190101113447901010-48 / 000
    private String sgkTitle;
    private String sgkAddress;
    private String sgkTaxNo;
    private String city;
    private String town;
    private int calculationStatus;
    public String sgkUserName; // LoginInfoDTO.username
    public String sgkUserNameAdd; // LoginInfoDTO.extension

    @Convert(converter = PasswordColumnConverter.class)
    private String sgkPassword; // LoginInfoDTO.systemPassword

    @Convert(converter = PasswordColumnConverter.class)
    private String sgkMerchantPassword; // LoginInfoDTO.companyPassword

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "userMerchant")
    private List<MerchantCalculationResult> merchantCalculationResult;
}
