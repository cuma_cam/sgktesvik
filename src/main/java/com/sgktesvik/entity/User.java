package com.sgktesvik.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name = "user_info")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_info_generator")
    @SequenceGenerator(name = "user_info_generator", sequenceName = "user_info_seq")
    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private Boolean isApproved;

    private String approvalGuid;

    private String phone;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<UserMerchant> merchants;
}
