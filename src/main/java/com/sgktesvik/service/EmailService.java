package com.sgktesvik.service;

import com.sgktesvik.service.dto.EmailDTO;

/**
 * Created on 31.03.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
public interface EmailService {
    void sendEmail(EmailDTO emailDTO);
}
