package com.sgktesvik.service;

import java.util.Map;

/**
 * Created on 31.03.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
public interface EmailContentBuilder {
    String build(Map<String, Object> parameters, String mailTemplate);
}
