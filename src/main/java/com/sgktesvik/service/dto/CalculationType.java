package com.sgktesvik.service.dto;

import java.util.Arrays;

/**
 * Created on 5.04.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
public enum CalculationType {

    DEMO("DEMO", "Demo"),
    FINAL("FINAL", "Gerçek");

    CalculationType(String code, String description) {
        this.code = code;
        this.description = description;
    }

    private String code;
    private String description;

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static CalculationType fromCode(String code) {
        return Arrays.stream(CalculationType.values()).filter(item->item.getCode().equals(code)).findFirst().orElse(null);
    }
}
