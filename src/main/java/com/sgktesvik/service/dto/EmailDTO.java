package com.sgktesvik.service.dto;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * Created on 31.03.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
@Data
public class EmailDTO {
    private String subject;

    private String from;

    private String to;

    private String templateName;

    private Map<String, Object> parameters;

    private List<String> attachmentList;
}
