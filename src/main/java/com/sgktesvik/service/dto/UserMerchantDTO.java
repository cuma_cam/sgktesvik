package com.sgktesvik.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.Serializable;

/**
 * Created on 4.04.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
@Data
public class UserMerchantDTO implements Serializable {
    private Long id;
    private Long userId;
    private String owner;
    private String sgkRegistryNoKey;
    private String sgkRegistryNo;
    private String sgkTitle;
    private String sgkAddress;
    private String sgkTaxNo;
    private String city;
    private String town;
    private String calculationStatus;
    private String sgkUserName;
    private String sgkUserNameAdd;
    @JsonIgnore
    private String sgkPassword;
    @JsonIgnore
    private String sgkMerchantPassword;
}
