package com.sgktesvik.service.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created on 3.04.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
@Data
public class UserDTO implements Serializable {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private Boolean isApproved;
    private String approvalGuid;
    private String phone;
    private List<UserMerchantDTO> merchants;
}
