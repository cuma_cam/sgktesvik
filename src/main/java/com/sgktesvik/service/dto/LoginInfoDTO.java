package com.sgktesvik.service.dto;

import java.io.Serializable;

/**
 *
 */
public class LoginInfoDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    public String userName;
    public String extension;
    public String systemPassword;
    public String companyPassword;

    @Override
    public boolean equals(Object otherObj){
        if(otherObj==null || (otherObj instanceof LoginInfoDTO) == false)
            return false;

        LoginInfoDTO other = (LoginInfoDTO) otherObj;

        return userName.compareTo(other.userName) == 0
                && extension.compareTo(other.extension) == 0
                && systemPassword.compareTo(other.systemPassword) == 0
                && companyPassword.compareTo(other.companyPassword) == 0 ;
    }

    @Override
    public int hashCode(){
        return (userName+extension+systemPassword+companyPassword).hashCode();
    }
}
