package com.sgktesvik.service.dto;

import java.util.Arrays;

public enum CalculationStatus {

    Draft(0, "Hesaplama Onayı Bekliyor"),
    WaitingCalculation(1, "Hesaplama Bekliyor"),
    Calculating(2, "Hesaplanıyor"),
    Calculated(3, "Hesaplandı");

    private int status;
    private String description;

    CalculationStatus(int status, String description){
        this.status = status;
        this.description = description;
    }

    public int getStatus(){
        return this.status;
    }
    public String getDescription() {return this.description;}
    public static CalculationStatus fromStatus(int status) {
        return Arrays.stream(CalculationStatus.values()).filter(item -> item.getStatus() == status).findFirst().orElse(null);
    }
}
