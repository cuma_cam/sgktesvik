package com.sgktesvik.service.dto.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created on 5.04.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
@Data
@NoArgsConstructor
public class MerchantCalculationResultDTO implements Serializable {
    private Long merchantId;
    private String calculationType;
    private BigDecimal refundAmount;
}
