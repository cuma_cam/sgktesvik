package com.sgktesvik.service.dto.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created on 5.04.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
@Data
@NoArgsConstructor
public class MerchantCredentialDTO implements Serializable{
    @JsonProperty("merchantId")
    private Long id;

    @JsonProperty("userName")
    private String sgkUserName;

    @JsonProperty("extension")
    private String sgkUserNameAdd;

    @JsonProperty("systemPassword")
    private String sgkPassword;

    @JsonProperty("companyPassword")
    private String sgkMerchantPassword;
}
