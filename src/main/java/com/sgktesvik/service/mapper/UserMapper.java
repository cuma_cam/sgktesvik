package com.sgktesvik.service.mapper;

import com.sgktesvik.entity.User;
import com.sgktesvik.entity.UserMerchant;
import com.sgktesvik.service.dto.UserDTO;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 3.04.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
@NoArgsConstructor
public final class UserMapper {

    public static UserDTO convertToDto(User entity) {
        if(entity != null) {
            UserDTO dto = new UserDTO();
            BeanUtils.copyProperties(entity,dto);
            if(!CollectionUtils.isEmpty(entity.getMerchants())) {
                dto.setMerchants(UserMerchantMapper.convertToDtoList(entity.getMerchants()));
            }
            return dto;
        }
        return null;
    }

    public static List<UserDTO> convertToDtoList(List<User> users) {
        if(!CollectionUtils.isEmpty(users)) {
            return users.stream().map(UserMapper::convertToDto).collect(Collectors.toList());
        }
        return null;
    }

    public static User convertToEntity(UserDTO dto) {
        if(dto != null) {
            User entity = new User();
            BeanUtils.copyProperties(dto, entity);
            if(!CollectionUtils.isEmpty(dto.getMerchants())) {
                List<UserMerchant> merchants = dto.getMerchants().stream().map(UserMerchantMapper::convertToEntity).collect(Collectors.toList());
                entity.setMerchants(merchants);
            }
            return entity;
        }
        return null;
    }
}
