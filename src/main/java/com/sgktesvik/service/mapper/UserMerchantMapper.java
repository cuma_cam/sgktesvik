package com.sgktesvik.service.mapper;

import com.sgktesvik.service.dto.CalculationStatus;
import com.sgktesvik.entity.UserMerchant;
import com.sgktesvik.service.dto.UserMerchantDTO;
import com.sgktesvik.service.dto.api.MerchantCredentialDTO;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created on 3.04.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
@NoArgsConstructor
public final class UserMerchantMapper {

    public static UserMerchantDTO convertToDto(UserMerchant entity) {
        if(entity != null) {
            UserMerchantDTO dto = new UserMerchantDTO();
            BeanUtils.copyProperties(entity,dto);
            dto.setUserId(entity.getUser().getId());
            dto.setCalculationStatus(CalculationStatus.fromStatus(entity.getCalculationStatus()).getDescription());
            dto.setOwner(MessageFormat.format("{0} {1}",entity.getUser().getFirstName(), entity.getUser().getLastName()));
            return dto;
        }
        return null;
    }

    public static List<UserMerchantDTO> convertToDtoList(List<UserMerchant> userMerchants) {
        if(!CollectionUtils.isEmpty(userMerchants)) {
            return userMerchants.stream().map(UserMerchantMapper::convertToDto).collect(Collectors.toList());
        }
        return null;
    }

    public static UserMerchant convertToEntity(UserMerchantDTO dto) {
        if(dto != null) {
            UserMerchant entity = new UserMerchant();
            BeanUtils.copyProperties(dto, entity);
            return entity;
        }
        return null;
    }

    public static MerchantCredentialDTO convertToMerchantCredentialDto(UserMerchant entity) {
        if(entity != null) {
            MerchantCredentialDTO dto = new MerchantCredentialDTO();
            BeanUtils.copyProperties(entity,dto);
            return dto;
        }
        return null;
    }
}
