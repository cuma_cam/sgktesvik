package com.sgktesvik.service.sgk;

import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.lept;
import org.bytedeco.javacpp.tesseract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static org.bytedeco.javacpp.lept.pixDestroy;
import static org.bytedeco.javacpp.lept.pixRead;

/**
 * Created by aarslan on 1/19/2017.
 */
@Component
class OcrSolver {

    Logger logger = LoggerFactory.getLogger(this.getClass());


    public String solve(String imagePath) throws Exception {
        String tesseractPath = ".";
        BytePointer outText;

        tesseract.TessBaseAPI api = new tesseract.TessBaseAPI();
        // Initialize tesseract-ocr with English, without specifying tessdata3 path

        logger.info("Using tesseract path:" + tesseractPath);
        if (api.Init(tesseractPath, "eng") != 0) {
            System.err.println("Could not initialize tesseract.");
           // throw new Exception("Could not initialize tesseract");
        }

        // Open input image with leptonica library
        lept.PIX image = pixRead(imagePath);


        api.SetImage(image);
        // Get OCR result
        outText = api.GetUTF8Text();

        // Destroy used object and release memory
        api.End();
        outText.deallocate();
        pixDestroy(image);
        logger.info(String.format("Catpcha %s olarak çözüldü", outText.getString().trim()));
        return outText.getString();
    }

}
