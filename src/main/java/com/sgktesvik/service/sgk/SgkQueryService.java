package com.sgktesvik.service.sgk;

import com.gargoylesoftware.htmlunit.*;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlImage;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.sgktesvik.service.dto.CalculationStatus;
import com.sgktesvik.service.dto.LoginInfoDTO;
import com.sgktesvik.entity.UserMerchant;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import java.io.File;
import java.io.IOException;

@Slf4j
public class SgkQueryService {
    public UserMerchant retrieveMerchantFromSgk(LoginInfoDTO loginInfo) {
        UserMerchant merchant = null;
        for (int i = 0; i < 3; i++) {
            merchant = tryRetrieveMerchantFromSgk(loginInfo);
            if(merchant!=null) return merchant;
        }
        return null;
    }

    private UserMerchant tryRetrieveMerchantFromSgk(LoginInfoDTO loginInfo){
        String loginUrl = "https://ebildirge.sgk.gov.tr/WPEB/amp/loginldap";
        OcrSolver ocrSolver = new OcrSolver();
        UserMerchant merchant = null;

        try(WebClient webClient = getNewWebClient()){
            HtmlPage htmlPage = null;
            int cnt = 0;
            while(htmlPage == null || htmlPage.getWebResponse().getStatusCode()!=200){
                try{htmlPage = webClient.getPage(loginUrl);}
                catch (Exception ex){
                    log.error("Login sayfası getirilemedi." + (cnt<5? "tekrar deneniyor":""));
                }
                if(cnt++ > 5)
                    return null;
            }

            HtmlForm loginForm = htmlPage.getFormByName("formA");
            loginForm.getInputByName("j_username").setValueAttribute( loginInfo.userName );
            loginForm.getInputByName("isyeri_kod").setValueAttribute( loginInfo.extension );
            loginForm.getInputByName("j_password").setValueAttribute( loginInfo.systemPassword );
            loginForm.getInputByName("isyeri_sifre").setValueAttribute( loginInfo.companyPassword );

            HtmlImage image = (HtmlImage)loginForm.<HtmlImage>getElementsByTagName("img").get(0);
            File tmpFile = File.createTempFile("captcha","image");
            image.saveAs(tmpFile);
            String ocrText = ocrSolver.solve(tmpFile.getAbsolutePath());

            loginForm.getInputByName("isyeri_guvenlik").setValueAttribute( ocrText.trim());

            loginForm.removeAttribute("onsubmit");
            loginForm.removeAttribute("target");
            loginForm.setAttribute("target","_self");

            Page page2 = loginForm.getInputByName("btnSubmit").click();
            merchant = getMerchant(page2.getWebResponse().getContentAsString());


            if(merchant == null) {
                log.error("Web client Login başarısız");
            } else {
                merchant.setSgkUserName(loginInfo.userName);
                merchant.setSgkPassword(loginInfo.systemPassword);
                merchant.setSgkMerchantPassword(loginInfo.companyPassword);
                merchant.setSgkUserNameAdd(loginInfo.extension);
                merchant.setCalculationStatus(CalculationStatus.Draft.getStatus());
                log.info("Web client Login tamamlandı.");
            }


        } catch (ScriptException scriptEx){
            log.error("Script exception oluştu.", scriptEx);
            scriptEx.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return merchant;
    }

    private UserMerchant getMerchant(String contentAsString) {

        Document document = Jsoup.parse(contentAsString);
        Elements elements = document.select("table[bgcolor=#000099] .p10");

        if(elements.size() < 3) return null;

        String sicilNo = elements.get(0).text().trim();
        sicilNo = sicilNo.substring(0, sicilNo.indexOf("(")).trim();
        String unvan = elements.get(1).text().trim();


        String adres = "";
        String il = "", ilce="";
        String vergiNo = "";

        try{
            String adresFull = elements.get(2).html().trim();
            if(adresFull!=null && adresFull.indexOf("Dış kapı no:")>0) {
                adres = adresFull.substring(0, adresFull.indexOf("Dış kapı no:"));
            }

            String ilIlce = adresFull.substring(adresFull.indexOf("<br>")+4).trim();

            if(ilIlce != null && ilIlce.length()>5){
                il =ilIlce.substring(0, ilIlce.indexOf(" "));
                ilce = ilIlce.substring(ilIlce.indexOf(" ")+1);
            }
        } catch (Exception ex){}
        elements = document.select("body > table:nth-child(5) > tbody > tr > td > table > tbody > tr:nth-child(2) > td:nth-child(2) > table:nth-child(3) > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(6) > td:nth-child(3) > font > table > tbody > tr:nth-child(1) > td:nth-child(6)");
        if(elements.size()>0){
            vergiNo = elements.first().text();
        }

        UserMerchant isyeri = new UserMerchant();
        isyeri.setSgkAddress(adres);
        isyeri.setCity(il);
        isyeri.setTown(ilce);
        isyeri.setSgkTaxNo(vergiNo);
        isyeri.setSgkRegistryNo(sicilNo);
        isyeri.setSgkTitle(unvan);
        isyeri.setSgkRegistryNoKey(getSicilNoKey(sicilNo));
        return isyeri;
    }

    private String getSicilNoKey(String sicilNo) {
        String isYeriSicilNoKey = "";
        for(char c : sicilNo.toCharArray()){
            if(Character.isDigit(c))
                isYeriSicilNoKey += c;
        }
        return isYeriSicilNoKey;
    }

    private WebClient getNewWebClient() {

        WebClient webClient = null;

        webClient = new WebClient(BrowserVersion.INTERNET_EXPLORER);
        webClient.getCookieManager().setCookiesEnabled(true);
        webClient.getOptions().setUseInsecureSSL(true);
        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
        webClient.getOptions().setThrowExceptionOnScriptError(false);
        webClient.setAjaxController(new NicelyResynchronizingAjaxController());
        webClient.getOptions().setCssEnabled(false);
        webClient.getOptions().setAppletEnabled(false);
        webClient.getOptions().setHistorySizeLimit(0);


        return webClient;
    }
}
