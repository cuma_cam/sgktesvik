package com.sgktesvik.service;

import com.sgktesvik.service.dto.CalculationStatus;
import com.sgktesvik.service.dto.UserDTO;
import com.sgktesvik.service.dto.UserMerchantDTO;
import com.sgktesvik.service.dto.api.MerchantCalculationResultDTO;
import com.sgktesvik.service.dto.api.MerchantCredentialDTO;

import java.util.List;

/**
 * Created on 3.04.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
public interface UserService {
    UserDTO createUser(UserDTO user);
    UserDTO getUser(Long id);
    UserMerchantDTO userAddMerchant(UserMerchantDTO userMerchantDTO);
    Boolean userDeleteMerchant(Long id);
    void userMerchantUpdateStatus(Long userId, CalculationStatus status);
    UserDTO findAndUpdateUserWithApprovalGuid(String approvalGuid);
    List<UserDTO> getUserList();
    MerchantCredentialDTO getNextMerchantCredential4Calculation(String calculationType);
    void saveMerchantCalculationResult(MerchantCalculationResultDTO result, byte[] zipFileData);
}
