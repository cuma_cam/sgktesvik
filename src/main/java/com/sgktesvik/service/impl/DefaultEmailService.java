package com.sgktesvik.service.impl;

import com.sgktesvik.service.dto.EmailDTO;
import com.sgktesvik.service.EmailContentBuilder;
import com.sgktesvik.service.EmailService;
import groovy.util.logging.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

/**
 * Created on 31.03.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
@Slf4j
@Service
public class DefaultEmailService implements EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private EmailContentBuilder emailContentBuilder;

    @Override
    public void sendEmail(EmailDTO email) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom(email.getFrom());
            messageHelper.setTo(email.getTo());
            messageHelper.setSubject(email.getSubject());
            String content = emailContentBuilder.build(email.getParameters(),email.getTemplateName());
            messageHelper.setText(content, true);
        };
        javaMailSender.send(messagePreparator);
    }
}
