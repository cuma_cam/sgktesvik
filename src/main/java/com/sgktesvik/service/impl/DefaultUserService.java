package com.sgktesvik.service.impl;

import com.sgktesvik.entity.MerchantCalculationResult;
import com.sgktesvik.repository.MerchantCalculationResultRepository;
import com.sgktesvik.service.dto.CalculationStatus;
import com.sgktesvik.service.dto.LoginInfoDTO;
import com.sgktesvik.entity.User;
import com.sgktesvik.entity.UserMerchant;
import com.sgktesvik.repository.UserMerchantRepository;
import com.sgktesvik.repository.UserRepository;
import com.sgktesvik.service.EmailService;
import com.sgktesvik.service.UserService;
import com.sgktesvik.service.dto.EmailDTO;
import com.sgktesvik.service.dto.UserDTO;
import com.sgktesvik.service.dto.UserMerchantDTO;
import com.sgktesvik.service.dto.api.MerchantCalculationResultDTO;
import com.sgktesvik.service.dto.api.MerchantCredentialDTO;
import com.sgktesvik.service.mapper.UserMapper;
import com.sgktesvik.service.mapper.UserMerchantMapper;
import com.sgktesvik.service.sgk.SgkQueryService;
import com.sgktesvik.util.KeyUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created on 3.04.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
@Service
public class DefaultUserService implements UserService {

    @Autowired
    EmailService emailService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserMerchantRepository userMerchantRepository;
    @Autowired
    private MerchantCalculationResultRepository merchantCalculationResultRepository;

    @Override
    public UserDTO createUser(UserDTO userDto) {
        userDto.setIsApproved(Boolean.FALSE);
        userDto.setApprovalGuid(KeyUtil.generateUuid());
        User user = userRepository.save(UserMapper.convertToEntity(userDto));
        sendApprovalEmail(userDto);
        return UserMapper.convertToDto(user);
    }

    @Override
    public UserDTO getUser(Long id) {
        return UserMapper.convertToDto(userRepository.findOne(id));
    }

    @Override
    public UserMerchantDTO userAddMerchant(UserMerchantDTO userMerchantDTO) {
        User user = userRepository.findOne(userMerchantDTO.getUserId());
        UserMerchant merchant = checkMerchant(userMerchantDTO);
        merchant.setUser(user);
        return UserMerchantMapper.convertToDto(userMerchantRepository.save(merchant));
    }

    @Override
    public Boolean userDeleteMerchant(Long id) {
        UserMerchant userMerchant = userMerchantRepository.findOne(id);
        if (userMerchant.getCalculationStatus() == CalculationStatus.Draft.getStatus()) {
            userMerchantRepository.delete(id);
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    @Override
    public void userMerchantUpdateStatus(Long userId, CalculationStatus status) {
        User user = userRepository.findOne(userId);
        UserDTO userDTO = UserMapper.convertToDto(user);
        user.getMerchants().stream().filter(item -> item.getCalculationStatus() == CalculationStatus.Draft.getStatus()).forEach(userMerchant -> {
            userMerchant.setCalculationStatus(status.getStatus());
            userMerchantRepository.save(userMerchant);
        });
        sendCalculationStartEmail(userDTO, CalculationStatus.Draft);
    }

    @Override
    public UserDTO findAndUpdateUserWithApprovalGuid(String approvalGuid) {
        User user = userRepository.findUserByApprovalGuid(approvalGuid);
        user.setIsApproved(Boolean.TRUE);
        userRepository.save(user);
        UserDTO userDto = UserMapper.convertToDto(user);
        sendApprovedInfoEmail(userDto);
        return userDto;
    }

    @Override
    public List<UserDTO> getUserList() {
        return UserMapper.convertToDtoList(userRepository.findAll());
    }

    @Override
    public MerchantCredentialDTO getNextMerchantCredential4Calculation(String calculationType) {
        UserMerchant userMerchant = userMerchantRepository.findFirstByCalculationStatusOrderByIdAsc(CalculationStatus.WaitingCalculation.getStatus());
        if (userMerchant != null) {
            userMerchant.setCalculationStatus(CalculationStatus.Calculating.getStatus());
            userMerchantRepository.save(userMerchant);
        }
        return UserMerchantMapper.convertToMerchantCredentialDto(userMerchant);
    }

    @Override
    public void saveMerchantCalculationResult(MerchantCalculationResultDTO result, byte[] zipFileData) {
        UserMerchant userMerchant = userMerchantRepository.findOne(result.getMerchantId());
        if(userMerchant != null){
            MerchantCalculationResult merchantCalculationResult = new MerchantCalculationResult();
            BeanUtils.copyProperties(result,merchantCalculationResult);
            merchantCalculationResult.setZipData(zipFileData);
            merchantCalculationResult.setUserMerchant(userMerchant);
            merchantCalculationResultRepository.save(merchantCalculationResult);
            userMerchant.setCalculationStatus(CalculationStatus.Calculated.getStatus());
            userMerchantRepository.save(userMerchant);
        }
    }

    private void sendCalculationStartEmail(UserDTO user, CalculationStatus status) {
        EmailDTO emailDTO = new EmailDTO();
        emailDTO.setSubject("Hesaplama Bildirim");
        emailDTO.setFrom("no-reply@tesvik.com");
        emailDTO.setTo(user.getEmail());
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("firstName", user.getFirstName());
        parameters.put("lastName", user.getLastName());
        parameters.put("email", user.getEmail());
        List<String> merchantNames = user.getMerchants().stream().filter(item -> item.getCalculationStatus().equals(status.getDescription()))
                .map(UserMerchantDTO::getSgkTitle).collect(Collectors.toList());
        parameters.put("merchants", merchantNames);
        emailDTO.setParameters(parameters);
        emailDTO.setTemplateName("calculation-start-mail-template");
        emailService.sendEmail(emailDTO);
    }

    private void sendApprovalEmail(UserDTO user) {
        EmailDTO emailDTO = new EmailDTO();
        emailDTO.setSubject("Onay Maili");
        emailDTO.setFrom("no-reply@tesvik.com");
        emailDTO.setTo(user.getEmail());
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("firstName", user.getFirstName());
        parameters.put("lastName", user.getLastName());
        String approvalLink = MessageFormat.format("localhost:8081/user/approve/{0}", user.getApprovalGuid());
        System.out.println(approvalLink);
        parameters.put("approvalLink", approvalLink);
        emailDTO.setParameters(parameters);
        emailDTO.setTemplateName("approval-mail-template");
        emailService.sendEmail(emailDTO);
    }

    private void sendApprovedInfoEmail(UserDTO user) {
        EmailDTO emailDTO = new EmailDTO();
        emailDTO.setSubject("Mailiniz onaylandı");
        emailDTO.setFrom("no-reply@tesvik.com");
        emailDTO.setTo(user.getEmail());
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("firstName", user.getFirstName());
        parameters.put("lastName", user.getLastName());
        String addMerchantLink = MessageFormat.format("localhost:8081/user/additional-info?id={0}", user.getId());
        System.out.println(addMerchantLink);
        parameters.put("addMerchantLink", addMerchantLink);
        emailDTO.setParameters(parameters);
        emailDTO.setTemplateName("approved-mail-template");
        emailService.sendEmail(emailDTO);
    }

    private UserMerchant checkMerchant(UserMerchantDTO userMerchantDTO) {
        SgkQueryService sgkQueryService = new SgkQueryService();
        LoginInfoDTO loginInfo = new LoginInfoDTO();
        loginInfo.userName = userMerchantDTO.getSgkUserName();//"16297865970";
        loginInfo.extension = userMerchantDTO.getSgkUserNameAdd();//"2";
        loginInfo.systemPassword = userMerchantDTO.getSgkPassword();//"270550";
        loginInfo.companyPassword = userMerchantDTO.getSgkMerchantPassword();//"270550";
        return sgkQueryService.retrieveMerchantFromSgk(loginInfo);
    }
}
