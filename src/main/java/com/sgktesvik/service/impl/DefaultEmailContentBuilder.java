package com.sgktesvik.service.impl;

import com.sgktesvik.service.EmailContentBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Map;

/**
 * Created on 31.03.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
@Service
public class DefaultEmailContentBuilder implements EmailContentBuilder{
    @Autowired
    private TemplateEngine templateEngine;

    @Override
    public String build(Map<String, Object> parameters, String mailTemplate) {
        Context context = new Context();
        parameters.entrySet().stream().forEach(item -> context.setVariable(item.getKey(),item.getValue()));
        return templateEngine.process(mailTemplate, context);
    }
}
