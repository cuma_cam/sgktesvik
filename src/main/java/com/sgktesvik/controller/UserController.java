package com.sgktesvik.controller;

import com.sgktesvik.service.RecaptchaService;
import com.sgktesvik.service.UserService;
import com.sgktesvik.service.dto.CalculationStatus;
import com.sgktesvik.service.dto.UserDTO;
import com.sgktesvik.service.dto.UserMerchantDTO;
import com.sgktesvik.service.dto.api.MerchantCalculationResultDTO;
import com.sgktesvik.service.dto.api.MerchantCredentialDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Transactional
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    RecaptchaService captchaService;

    @PostMapping("/user")
    public ResponseEntity<?> saveUser(@Valid UserDTO user, @RequestParam(name = "g-recaptcha-response") String recaptchaResponse, HttpServletRequest request) {

        String ip = request.getRemoteAddr();
        String captchaVerifyMessage = captchaService.verifyRecaptcha(ip, recaptchaResponse);
        Map<String, Object> response = new HashMap<>();
        if (StringUtils.isNotEmpty(captchaVerifyMessage)) {
            response.put("message", captchaVerifyMessage);
            return ResponseEntity.badRequest().body(response);
        }
        UserDTO userDTO = userService.createUser(user);
        response.put("id", userDTO.getId());
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/user/{id}")
    public ModelAndView getUser(@PathVariable("id") Long id) {
        ModelAndView mavUser = new ModelAndView("contact-save-success");
        mavUser.setStatus(HttpStatus.OK);
        mavUser.addObject("user", userService.getUser(id));
        return mavUser;
    }

    @PostMapping("/user/merchant/add")
    public ResponseEntity<?> updateUserAdditionalInfo(UserMerchantDTO merchantDTO) {
        Map<String, Object> response = new HashMap<>();
        response.put("userId", userService.userAddMerchant(merchantDTO).getUserId());
        return ResponseEntity.ok().body(response);
    }

    @DeleteMapping("/user/merchant/{id}")
    public ResponseEntity<?> deleteUserMerchant(@PathVariable("id") Long id) {
        if (!userService.userDeleteMerchant(id)) {
            Map<String, Object> response = new HashMap<>();
            response.put("message", MessageFormat.format("{0} statüsünde olmayan işletmeler silinemez!", CalculationStatus.Draft.getDescription()));
            return ResponseEntity.badRequest().body(response);
        }
        else {
            return ResponseEntity.ok().build();
        }
    }

    @PostMapping("/user/{userId}/merchant")
    public ResponseEntity<?> updateUserMerchantCalculationStatusToWaitingCalculation(@PathVariable("userId") Long userId) {
        userService.userMerchantUpdateStatus(userId, CalculationStatus.WaitingCalculation);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/user/additional-info")
    public ModelAndView getUser(UserDTO user) {

        UserMerchantDTO merchant = new UserMerchantDTO();
        merchant.setUserId(userService.getUser(user.getId()).getId());
        ModelAndView mavMerchant = new ModelAndView("additional-info");
        mavMerchant.setStatus(HttpStatus.OK);
        mavMerchant.addObject("merchant", merchant);
        return mavMerchant;
    }

    @GetMapping("/user/approve/{guid}")
    public ModelAndView approveUser(@PathVariable("guid") String guid) {
        UserDTO user = userService.findAndUpdateUserWithApprovalGuid(guid);
        ModelAndView approvalMav = new ModelAndView("approval");
        approvalMav.setStatus(HttpStatus.OK);
        approvalMav.addObject("user", user);
        return approvalMav;
    }

    @GetMapping("/users")
    public ResponseEntity<List<UserDTO>> users() {
        return ResponseEntity.ok(userService.getUserList());
    }

    @GetMapping("/user/{id}/merchants")
    public ResponseEntity<List<UserMerchantDTO>> userMerchants(@PathVariable("id") Long id) {
        return ResponseEntity.ok(userService.getUser(id).getMerchants());
    }

    @GetMapping("/user/merchant/credential")
    public MerchantCredentialDTO getNextMerchantCredential(String calculationType) {
        return userService.getNextMerchantCredential4Calculation(calculationType);
    }

    @PostMapping("user/merchant/uploadfile")
    public ResponseEntity<?> uploadCalculationResult(MerchantCalculationResultDTO calculationResult,
            @RequestParam(name = "zipFile") MultipartFile zipFile) {
        if(!zipFile.isEmpty()) {
            try {
                userService.saveMerchantCalculationResult(calculationResult,zipFile.getBytes());
                return ResponseEntity.ok().build();
            }
            catch (IOException e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        }
        return ResponseEntity.badRequest().build();
    }
}
