package com.sgktesvik.controller;

import com.sgktesvik.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewController {

    @GetMapping("/contact")
    public String contact(Model model) {
        model.addAttribute("user", new User());
        return "contact";
    }

    @GetMapping("/")
    public String index() {
        return "index";
    }
}
