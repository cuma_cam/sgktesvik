package com.sgktesvik.util;

import lombok.NoArgsConstructor;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.UUID;

/**
 * Created on 31.03.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
@NoArgsConstructor
public final class KeyUtil {
    private static final String ALGORITHM = "AES";
    private static final String keyValue = "ThisIsASecretKey";

    public static String generateUuid() {
        try {
            String uuid = UUID.randomUUID().toString();
            return uuid;
        } catch (Exception e) {
            return null;
        }
    }

    public static byte[] encrypt(String valueToEnc) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.ENCRYPT_MODE, key);
        return c.doFinal(valueToEnc.getBytes());
    }

    public static String decrypt(byte[] encryptedValue) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decValue = c.doFinal(encryptedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    }

    private static Key generateKey() throws Exception {
        Key key = new SecretKeySpec(keyValue.getBytes(), ALGORITHM);
        return key;
    }
}
