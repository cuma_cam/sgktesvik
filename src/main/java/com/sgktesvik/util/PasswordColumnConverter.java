package com.sgktesvik.util;

import javax.persistence.AttributeConverter;

/**
 * Created on 4.04.2018 in sgk-tesvik.
 *
 * @author Cuma Cam
 */
public class PasswordColumnConverter implements AttributeConverter<String, byte[]> {

    @Override
    public byte[] convertToDatabaseColumn(String s) {
        if(s == null || s.isEmpty())
        {
            return null;
        }
        try {
            return KeyUtil.encrypt(s);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String convertToEntityAttribute(byte[] bytes) {
        if(bytes == null || bytes.length < 1) {
            return null;
        }
        try {
            return KeyUtil.decrypt(bytes);
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
