$(function () {
    var userId = $("#userId").val();
    getUserMerchants(userId);
    var formId = "#user-additional-info-save-form";
    $(formId).validate({
        errorClass: "is-invalid",
        validClass: "is-valid",
        errorPlacement: function(error, element) {
            // Don't show error
        }
    });

    $(formId).submit(function () {
        if (!$("#allowPasswordSave").is(":checked")) {
            toastr.error("Devam etmeden önce şifreleri saklamamıza izin vermelisiniz!");
            return false;
        }
    });

    $("table#merchants-table").on("click", ".remove", function () {
        var merchantId = $(this).closest('tr').children('td:eq(0)').text();
        $.ajax({
            url: '/user/merchant/' + merchantId,
            type: 'DELETE',
            success: function(response) {
                toastr.success("İşletme Silindi");
                $(this).closest('tr').remove();
            },
            error: function (response) {
                if (response.responseJSON) {
                    toastr.error(response.responseJSON.message);
                } else {
                    toastr.error(response.responseText);
                }
            }
        });
    });

    $("#start-calculate-button").click(function(event) {
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: "/user/"+userId+"/merchant",
            success: function() {
               getUserMerchants(userId);
            },
            error: function(response) {
                if (response.responseJSON) {
                    toastr.error(response.responseJSON.message);
                } else {
                    toastr.error(response.responseText);
                }
            }
        });
    })

    $(formId).ajaxForm({
        beforeSubmit: function () {
            return $(formId).valid();
        },
        success: function (response) {
            getUserMerchants(response.userId);
            clearForm();
        },
        error: function (response) {
            if (response.responseJSON) {
                toastr.error(response.responseJSON.message);
            } else {
                toastr.error(response.responseText);
            }
        }
    });

});

function clearForm() {
    $("#sgkUserName").val('');
    $("#sgkUserNameAdd").val('');
    $("#sgkPassword").val('');
    $("#sgkMerchantPassword").val('');
    $("#allowPasswordSave").prop('checked', false);
}

function getUserMerchants(userId) {
    $.getJSON("/user/"+userId +"/merchants", function (data) {
        $("#merchant-list").html(Mustache.to_html($("#merchant-list-template").html(), data));
    });
}