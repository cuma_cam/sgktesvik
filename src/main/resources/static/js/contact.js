$(function () {
    var formId = "#user-save-form";
    $(formId).validate({
        errorClass: "is-invalid",
        validClass: "is-valid",
        errorPlacement: function(error, element) {
            // Don't show error
        }
    });

    $(formId).ajaxForm({
        beforeSubmit: function () {
            return $(formId).valid();
        },
        success: function (response) {
            window.location.href="user/" + response.id;
        },
        error: function (response) {
            if (response.responseJSON) {
                toastr.error(response.responseJSON.message);
            } else {
                toastr.error(response.responseText);
            }
            grecaptcha.reset();
        }
    });
});
