DROP SCHEMA IF EXISTS tesvikdb CASCADE;
CREATE SCHEMA tesvikdb;

SET SEARCH_PATH TO tesvikdb;

DROP TABLE IF EXISTS user_info;
DROP SEQUENCE IF EXISTS user_info_seq;

CREATE SEQUENCE user_info_seq;
CREATE TABLE user_info
(
  id BIGINT DEFAULT nextval('user_info_seq'::regclass) PRIMARY KEY NOT NULL,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(50) NOT NULL,
  email VARCHAR(128) NOT NULL,
  phone VARCHAR(11) NOT NULL,
  approval_guid VARCHAR(255),
  is_approved BOOLEAN
);
CREATE INDEX user_info_approval_guid_idx ON tesvikdb.user_info (approval_guid);

DROP TABLE IF EXISTS user_merchant;
DROP SEQUENCE IF EXISTS user_merchant_seq;

CREATE SEQUENCE user_merchant_seq;
CREATE TABLE user_merchant
(
  id BIGINT DEFAULT nextval('user_merchant_seq'::regclass) PRIMARY KEY NOT NULL,
  user_id BIGINT NOT NULL,
  sgk_registry_no_key VARCHAR(50),
  sgk_registry_no VARCHAR(50),
  sgk_title VARCHAR(255),
  sgk_address VARCHAR(255),
  sgk_tax_no VARCHAR(50),
  city VARCHAR(50),
  town VARCHAR(50),
  calculation_status INT,
  sgk_user_name VARCHAR(255),
  sgk_user_name_add VARCHAR(255),
  sgk_password BYTEA,
  sgk_merchant_password BYTEA,
  CONSTRAINT fk_user_merchant_user_info FOREIGN KEY (user_id) REFERENCES user_info (id)
);

DROP TABLE IF EXISTS merchant_calculation_result;
DROP SEQUENCE IF EXISTS merchant_calculation_result_seq;

CREATE SEQUENCE merchant_calculation_result_seq;
CREATE TABLE merchant_calculation_result
(
  id BIGINT DEFAULT nextval('merchant_calculation_result_seq'::regclass) PRIMARY KEY NOT NULL,
  user_merchant_id BIGINT NOT NULL,
  calculation_type VARCHAR(10) NOT NULL,
  refund_amount NUMERIC(18,2),
  zip_data BYTEA,
  CONSTRAINT fk_merchant_calculation_result_user_merchant FOREIGN KEY (user_merchant_id) REFERENCES user_merchant (id)
);